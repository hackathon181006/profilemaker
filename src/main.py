from server import Server
from source_parser import VKSourceParser, InstagramSourceParser

server_configuration = {
    'host': '0.0.0.0',
    'port': 8089,
    'service': '/get_mprofile/',
    'mongo_host': 'localhost',
    'mongo_port': 27017,
    'mongo_db': 'MegafonHackathon',
    'mongo_collection': 'Users'
}

def run_app():
    source_parsers = [
        VKSourceParser(),
        InstagramSourceParser()
    ]
    server = Server(server_configuration, source_parsers)
    server.run()


if __name__ == "__main__":
    run_app()
