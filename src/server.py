from aiohttp import web
from pymongo import MongoClient
from utils import log


class Server:

    def __init__(self, configuration, source_parsers):
        self._sps = source_parsers
        # app
        self._app = web.Application()
        self._app.router.add_post(configuration['service'], self.handler)
        self._host = configuration['host']
        self._port = configuration['port']
        # mongo
        self._client = MongoClient(configuration['mongo_host'], configuration['mongo_port'])
        self._db = self._client[configuration['mongo_db']]
        self._collection = self._db[configuration['mongo_collection']]

    def run(self):
        """
        Run the server
        """
        web.run_app(self._app, host=self._host, port=self._port)

    async def handler(self, request):
        """
        Handler
        """
        req_data = await request.json()
        log('request', req_data)

        err_msg = ''
        if 'source' not in req_data:
            err_msg = 'No parameter "source"!'
        if 'id' not in req_data:
            err_msg = 'No parameter "id"!'
        if err_msg != '':
            log('response', 400, err_msg)
            return web.json_response(err_msg, status=400)

        uid = req_data['id']
        src = req_data['source']

        if type(src) != int or src < 0 or src > len(self._sps):
            err_msg = 'Bad parameter "source"!'
            log('response', 400, err_msg)
            return web.json_response(err_msg, status=400)

        user_key = str(src) + str(uid)
        cursor = self._collection.find({'key': user_key})
        cursor = list(cursor)
        if len(cursor) > 0:
            multiprofile = cursor[0]
            multiprofile['uid'] = str(multiprofile['_id'])
            del multiprofile['_id']
            log('response (cached)', multiprofile)
            return web.json_response(multiprofile, status=200)

        # определить основной профиль
        profile = await self._sps[src].find(uid)
        multiprofile = {'key': user_key,
                        'source': src,
                        'user_profile': profile,
                        'similar_profiles': []}
        # определить мультипрофили
        similar_id_dict = {}
        for sp_id in range(len(self._sps)):
            if sp_id == src:
                continue
            similar_data = await self._sps[sp_id].similar(uid)
            if similar_data is None:
                continue
            similar_profile = similar_data['profile']
            similar_key = str(sp_id)+similar_profile['username']
            #попытаться найти смежный профиль в БД
            cursor = self._collection.find({'key': similar_key})
            cursor = list(cursor)
            similar = None
            if len(cursor) > 0:
                similar_id = str(cursor[0]['_id'])
                similar_id_dict[similar_id] = cursor[0]['_id']
                similar = {
                    'mprofile_id': similar_id,
                    'probability': similar_data['probability']
                }
            if similar is None:
                # добавить смежный профиль в БД
                similar_multiprofile = {
                    'key': similar_key,
                    'source': sp_id,
                    'user_profile': similar_profile,
                    'similar_profiles': []
                }
                self._collection.insert_one(similar_multiprofile)
                similar_id = str(similar_multiprofile['_id'])
                similar_id_dict[similar_id] = similar_multiprofile['_id']
                similar = {
                    'mprofile_id': similar_id,
                    'probability': similar_data['probability']
                }
            multiprofile['similar_profiles'].append(similar)

        # сохранение мультипрофиля
        self._collection.insert_one(multiprofile)
        multiprofile_id = multiprofile['_id']
        multiprofile['uid'] = str(multiprofile_id)
        del multiprofile['_id']

        for similar in multiprofile['similar_profiles']:
            added = {
                'mprofile_id': str(multiprofile_id),
                'probability': similar['probability']
            }
            self._collection.find_one_and_update(
                {'_id': similar_id_dict[similar['mprofile_id']]},
                {'$push': {'similar_profiles': added}})

        log('response', multiprofile)
        return web.json_response(multiprofile, status=200)
