from abc import ABCMeta, abstractmethod
import aiohttp
import json
from aiohttp.client_exceptions import ContentTypeError
from utils import log


class SourceParser:
    """ Base class for source parcers
    """

    __metaclass__ = ABCMeta

    @abstractmethod
    async def find(self, uid):
        raise NotImplementedError

    @abstractmethod
    async def similar(self, user):
        raise NotImplementedError

    @abstractmethod
    def adapt_to_general(self, user):
        raise NotImplementedError

    # def adapt_to_specific(self, user):
    #     raise NotImplementedError


class VKSourceParser(SourceParser):

    def __init__(self):
        self.user_adapter = {'username': 'username'}

    async def find(self, uid):
        # res = {'username': 'Bob'}
        # return res
        async with aiohttp.ClientSession() as session:
            async with session.get(f"http://localhost:8124/get-user?uid={uid}") as resp:
                try:
                    data = await resp.text()
                    data = json.loads(data)
                except ContentTypeError as e:
                    data = e.message
        return self.adapt_to_general(data)

    async def similar(self, user):
        return None
        # specific_user = self.adapt_to_specific(user)
        # # TODO res = await from service
        # res = {'profile': {'username': 'Bobb'},
        #        'probability': 0.6}
        # return {'profile': self.adapt_to_general(res['profile']),
        #        'probability': res['probability']}

    def adapt_to_general(self, user):
        return {
            'id': user['id'] if 'id' in user else None,
            'username': user['screen_name'] if 'screen_name' in user else None,
            'fullname': (user['first_name'] if 'first_name' in user else None) +
                        (user['last_name'] if 'last_name' in user else None),
            'follower_count': user['followers_count'] if 'followers_count' in user else None,
            'phone_number': user['mobile_phone'] if 'mobile_phone' in user else None,
            'city_name': user['city']['title'] if 'city' in user and 'title' in user['city'] else None,
            'country_code': user['country']['id'] if 'country' in user and 'id' in user['country'] else None,
            'gender': user['sex'] if 'sex' in user else None,
            'birthday': user['bdate'] if 'bdate' in user else None,
        }


class InstagramSourceParser(SourceParser):

    def __init__(self):
        self.user_adapter = {'username': 'username'}

    async def find(self, uid):
        # res = {'username': 'Alex'}
        # return res
        async with aiohttp.ClientSession() as session:
            async with session.get(f"http://localhost:8123/get-by-username?username={uid}") as resp:
                try:
                    data = await resp.text()
                    data = json.loads(data)
                except ContentTypeError as e:
                    data = e.message
        return self.adapt_to_general(data)

    async def similar(self, user):
        return None
        specific_user = self.adapt_to_specific(user)
        # # TODO res = await from service
        # res = {'profile': {'username': 'Alexx'},
        #        'probability': 0.6}
        # return {'profile': self.adapt_to_general(res['profile']),
        #        'probability': res['probability']}

    def adapt_to_general(self, user):
        return {
            'id': user['pk'] if 'pk' in user else None,
            'username': user['username'] if 'username' in user else None,
            'fullname': user['fullname'] if 'fullname' in user else None,
            'follower_count': user['follower_count'] if 'follower_count' in user else None,
            'phone_number': user['phone_number'] if 'phone_number' in user else None,
            'city_name': user['city_name'] if 'city_name' in user else None,
            'country_code': user['country_code'] if 'country_code' in user else None,
            'gender': user['gender'] if 'gender' in user else None,
            'birthday': user['birthday'] if 'birthday' in user else None,
        }
